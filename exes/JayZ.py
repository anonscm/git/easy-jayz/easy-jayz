#!/usr/bin/env python3
import time
import math
import os
import optparse
import re
import sys
import subprocess
from subprocess import *
import numpy as np
from operator import *
import inspect

import Bio
from Bio import *
from Bio.PDB import *

import multiprocessing
from multiprocessing import Pool,Process
import joblib
from joblib import Parallel,delayed

import pyrosetta
from pyrosetta import *
from pyrosetta.toolbox import *

import pyrosetta.rosetta
from pyrosetta.rosetta import *
from pyrosetta.rosetta.core.scoring import *
from pyrosetta.rosetta.core.scoring.constraints import *
from pyrosetta.rosetta.core.pack.rotamer_set import *
from pyrosetta.rosetta.core.pack.interaction_graph import *
from pyrosetta.rosetta.core.pack import *
from pyrosetta.rosetta.protocols.simple_moves import * 
from pyrosetta.rosetta.protocols.relax import *
from pyrosetta.rosetta.core.pack.task import *

from pyrosetta.rosetta.protocols.rigid import *
from pyrosetta.rosetta.protocols.scoring import Interface
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

class bcolors:
    BRED = '\x1b[1;31;40m'
    BBLUE = '\x1b[1;34;40m'
    BGREEN = '\x1b[1;32;40m'
    ENDC = '\x1b[0m'
    BOLD = '\x1b[1m'
    UNDERLINE = '\x1b[4m'

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', time=-1,decimals = 1, length = 100, fill = bcolors.BGREEN+'█'+bcolors.ENDC):
        
    # Call in a loop to create terminal progress bar
    # @params:
    #     iteration   - Required  : current iteration (Int)
    #     total       - Required  : total iterations (Int)
    #     prefix      - Optional  : prefix string (Str)
    #     decimals    - Optional  : positive number of decimals in percent complete (Int)
    #     length      - Optional  : character length of bar (Int)
    #     fill        - Optional  : bar fill character (Str)

    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength +(bcolors.BGREEN+'-'+bcolors.ENDC) * (length - filledLength)
    if time < 0:
        print('\r'+prefix+' '+percent+'% |'+bar+'|' , end='\r')
    else:
        print('\r'+prefix+' '+percent+'% |'+bar+'| ', end = "Remaining approximatively "+hmsTime(time)+'\r')
    # Print New Line on Complete
    if iteration == total:
        print()
def minimized(pose,score,min_steps,bb_cst_sd):
    copy_pose = Pose()
    copy_pose.assign(pose)

    movemap = MoveMap()
    movemap.set_chi(True)
    best_pose=Pose()
    best_pose.assign(copy_pose)
    score_fxn=create_score_function(score)
        
    if bb_cst_sd>0:
        movemap.set_bb(True) ## Move Backbone
        add_coordinate_constraints(copy_pose, bb_cst_sd) #coordinate constraint with distance dist.
        score_fxn.set_weight(coordinate_constraint,1) #set the coordinate constraint weight to 1        
    else:
        movemap.set_bb(False)
        
    tolerance = 0.001 ## Set the tolerance, minimum will be in tolerance of a local min
    min_type = "dfpmin" ## Quasi Newton
    minmover = MinMover(movemap, score_fxn, min_type, tolerance, True)
    for i in range(0,min_steps):
        if bb_cst_sd>0 and verbosity>0:
            print("Doing "+str(i+1)+"th side-chain and backbone minimization with "+score)
        elif bb_cst_sd == 0 and verbosity>0:
            print( "Doing "+str(i+1)+"th side-chain minimization with "+score)
        minmover.apply(copy_pose)
        if verbosity > 1 :
            print("Best pose:",score_fxn(best_pose),"Actual min pose:",score_fxn(copy_pose))
        if score_fxn(copy_pose) < score_fxn(best_pose):
            best_pose.assign(copy_pose)
    return best_pose

def fast_relaxed(pose,score_fxn,fast_relax):
    relax=FastRelax(score_fxn)
    for i in range(0,fast_relax):
        if verbosity > 0:
            print("Doing "+str(i+1)+"th Fast relax")
        relax.apply(pose)
    return pose

def Separate_complex(pdb,chain_name,folder):
    pdbio = PDBIO()
    pdbio.set_structure(pdb)
    receptor_chain=list(chain_name[0])
    ligand_chain=list(chain_name[1])
    
    class Receptor_selector(Select):
        def accept_chain(self,chain):
            if chain.get_id() in receptor_chain:
                return 1
            else:
                return 0

    class Ligand_selector(Select):
        def accept_chain(self,chain):
            if chain.get_id() in ligand_chain:
                return 1
            else:
                return 0
    pdbio.save(folder+pdb.get_id() + "_" + chain_name[0]+ ".pdb",Receptor_selector())
    pdbio.save(folder+pdb.get_id() + "_" + chain_name[1]+ ".pdb",Ligand_selector())

    if os.stat(folder+pdb.get_id() + "_" + chain_name[0]+ ".pdb").st_size==0:
        print(bcolors.BRED+'ERROR the file '+folder+pdb.get_id() + "_" + chain_name[0]+ ".pdb is empty"+bcolors.ENDC)
        print(bcolors.BRED+'Check if the partner are set right'+bcoors.ENDC)
        print(bcolors.BRED+'Exit....')
        exit(0)
    elif os.stat(folder+pdb.get_id() + "_" + chain_name[1]+ ".pdb").st_size==0:
        print(bcolors.BRED+'ERROR the file '+folder+pdb.get_id() + "_" + chain_name[1]+ ".pdb is empty"+bcolors.ENDC)
        print(bcolors.BRED+'Check if the partner are set right'+bcoors.ENDC)
        print(bcolors.BRED+'Exit....')
        exit(0)

        
def extract_interactions(pose,resfile,out,scorefxn):
    copy_pose=Pose()
    copy_pose.assign(pose)
    task_design = TaskFactory.create_packer_task(copy_pose)
    task_design.initialize_from_command_line()
    parse_resfile(copy_pose, task_design, resfile)
    copy_pose.update_residue_neighbors()
    png = create_packer_graph(copy_pose, scorefxn, task_design)
    rotsets = RotamerSets()
    pack_rotamers_setup(copy_pose, scorefxn, task_design, rotsets)
    ig = InteractionGraphFactory.create_and_initialize_two_body_interaction_graph(task_design, rotsets, copy_pose, scorefxn, png)
    if verbosity > 1:
        print("Design:",task_design.design_any())
        print("Residue design vector:")
        print(task_design.designing_residues())
             
    out_LG=out+'.LG'
    out_uai=out+'.uai'
    g = open(out_LG,'w')
    h = open(out_uai,'w')
    g.write("MARKOV\n")
    g.write(str(ig.get_num_nodes())+'\n')
    h.write("MARKOV\n")
    h.write(str(ig.get_num_nodes())+'\n')
    domain=StringIO()
    scope=StringIO()
    unary_terms=StringIO()
    exp_unary_terms=StringIO()
    binary_terms=StringIO()
    exp_binary_terms=StringIO()
    seq_AA_rot=StringIO()
    number_of_functions=0
    for res1 in range(1,ig.get_num_nodes()+1):
        number_of_functions += 1
        domain_res=0
        unary=[]
        exp_unary=[]
        scope.write("1 "+str(res1-1)+'\n')
        for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
            resname = rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name1()
            seq_AA_rot.write(resname)
            ener=ig.get_one_body_energy_for_node_state(res1,i)
            unary.append(str(-ener))  # TS le LG, on met oppose du log
            exp_unary.append(str(np.exp(-ener)))
            domain_res = domain_res + 1
        seq_AA_rot.write("\n")
        unary_terms.write(str(domain_res)+'\n')
        unary_terms.write(" ".join(unary)+'\n')
        exp_unary_terms.write(str(domain_res)+'\n')
        exp_unary_terms.write(" ".join(exp_unary)+'\n')
        domain.write(str(domain_res)+' ')
    domain.write('\n')

    for res1 in range(1,ig.get_num_nodes()+1):
        for res2 in range(res1+1,ig.get_num_nodes()+1):
            if (ig.get_edge_exists(res1, res2)):
                number_of_functions += 1
                scope.write("2 "+str(res1-1)+" "+str(res2-1)+"\n")
                domain_res=0
                binary=[]
                exp_binary=[]
                for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
                    resname1 = rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name1()
                    resnum1 = rotsets.rotamer_set_for_moltenresidue(res1).resid()
                    for j in range(1, rotsets.rotamer_set_for_moltenresidue(res2).num_rotamers()+1):
                        resname2 = rotsets.rotamer_set_for_moltenresidue(res2).rotamer(j).name1()
                        resnum2 = rotsets.rotamer_set_for_moltenresidue(res2).resid()
                        ener=ig.get_two_body_energy_for_edge(res1,res2,i,j)
                        binary.append(str(-ener))
                        exp_binary.append(str(np.exp(-ener)))
                        domain_res=domain_res+1
                    binary.append('\n')
                    exp_binary.append('\n')

                binary_terms.write(str(domain_res)+'\n')
                exp_binary_terms.write(str(domain_res)+'\n')
                for i in range(0,len(binary)) :
                    if i != len(binary)-1 and binary[i] != '\n':
                        binary_terms.write(binary[i]+" ")
                        exp_binary_terms.write(exp_binary[i]+" ")
                    elif binary[i] == '\n':
                        binary_terms.write(binary[i])
                        exp_binary_terms.write(exp_binary[i])

    g.write(domain.getvalue())
    h.write(domain.getvalue())
    g.write(str(number_of_functions)+'\n')
    h.write(str(number_of_functions)+'\n')
    g.write(scope.getvalue())
    h.write(scope.getvalue())
    
    g.write(unary_terms.getvalue())
    g.write(binary_terms.getvalue())

    h.write(exp_unary_terms.getvalue())
    h.write(exp_binary_terms.getvalue())

    g.write(seq_AA_rot.getvalue())
    h.write(seq_AA_rot.getvalue())

    domain.close()
    scope.close()
    unary_terms.close()
    binary_terms.close()
    exp_unary_terms.close()
    exp_binary_terms.close()
    g.close()
    h.close()

def extract_sequence_file(pose,out,mut,nres_rec,nres_lig,nres_tot):
    r = re.compile("([0-9]+)([a-zA-Z]+)")
    target_sequence=pose.sequence()
    if not mut == pdb_file.split('.pdb')[0]:
        for mutation in mut.split('_'):
            name_pos_mutAA=r.match(mutation).groups()
            if len(target_sequence) == nres_tot:
                target_sequence=target_sequence[:int(name_pos_mutAA[0])-1]+name_pos_mutAA[1]+target_sequence[int(name_pos_mutAA[0]):]
            if len(target_sequence) == nres_rec and int(name_pos_mutAA[0]) <= nres_rec:
                target_sequence=target_sequence[:int(name_pos_mutAA[0])-1]+name_pos_mutAA[1]+target_sequence[int(name_pos_mutAA[0]):]
            if len(target_sequence) == nres_lig and nres_rec < int(name_pos_mutAA[0]):
                pos_renum=int(name_pos_mutAA[0])-nres_rec
                target_sequence=target_sequence[:pos_renum-1]+name_pos_mutAA[1]+target_sequence[pos_renum:]
    outfile=open(out,'w')
    outfile.write(target_sequence)
    outfile.close()
    
def extract_sequence_file_opt(pose,out,mut,nres_rec,nres_lig,nres_tot,flexibles):
    r = re.compile("([0-9]+)([a-zA-Z]+)")
    target_sequence=pose.sequence()
    if not mut == pdb_file.split('.pdb')[0]:
        for mutation in mut.split('_'):
            name_pos_mutAA=r.match(mutation).groups()
            if len(target_sequence) == nres_tot:
                target_sequence=target_sequence[:int(name_pos_mutAA[0])-1]+name_pos_mutAA[1]+target_sequence[int(name_pos_mutAA[0]):]
            if len(target_sequence) == nres_rec and int(name_pos_mutAA[0]) <= nres_rec:
                target_sequence=target_sequence[:int(name_pos_mutAA[0])-1]+name_pos_mutAA[1]+target_sequence[int(name_pos_mutAA[0]):]
            if len(target_sequence) == nres_lig and nres_rec < int(name_pos_mutAA[0]):
                pos_renum=int(name_pos_mutAA[0])-nres_rec
                target_sequence=target_sequence[:pos_renum-1]+name_pos_mutAA[1]+target_sequence[pos_renum:]
    tronc_seq=""
    for i in range(0,len(target_sequence)):
        if i+1 in flexibles:
            tronc_seq  = tronc_seq +  target_sequence[i]
    outfile=open(out,'w')
    outfile.write(tronc_seq)
    outfile.close()

def create_resfile(pose,pose_rec,pose_lig,sequences,Mut_on_rec,Mut_on_lig,folder):
    copy_pose=Pose()
    copy_pose.assign(pose)
    
    target_sequence=copy_pose.sequence()
    
    nres_first_chain=int(pose_rec.pdb_info().nres())
    resfile=open(folder+pose.pdb_info().name().split('.pdb')[0]+'.resfile','w')
    resfile.write("NATAA\n")
    resfile.write("USE_INPUT_SC\n")
    resfile.write("start\n")

    if Mut_on_rec:
        resfile_rec=open(pose_rec.pdb_info().name().split('.pdb')[0]+'.resfile','w')
        resfile_rec.write("NATAA\n")
        resfile_rec.write("USE_INPUT_SC\n")
        resfile_rec.write("start\n")
    if Mut_on_lig:
        resfile_lig=open(pose_lig.pdb_info().name().split('.pdb')[0]+'.resfile','w')
        resfile_lig.write("NATAA\n")
        resfile_lig.write("USE_INPUT_SC\n")
        resfile_lig.write("start\n")

    dic_res={}
    r = re.compile("([0-9]+)([a-zA-Z]+)")
    for mut in sequences:
        if not mut == pdb_file.split('.pdb')[0]:
            for mutation in mut.split('_'):
                name_pos=r.match(mutation).groups()
                if name_pos[0] in dic_res.keys():
                    dic_res[name_pos[0]] += name_pos[1]
                else:
                    dic_res[name_pos[0]] = name_pos[1]
    
    for pos in dic_res.keys():
        dic_res[pos] += target_sequence[int(pos)-1]
        chain=pose.pdb_info().chain(int(pos))
        resfile.write(pos+" "+chain+" USE_INPUT_SC PIKAA "+dic_res[pos]+'\n')
    resfile.close()

    if Mut_on_rec:
        for pos in dic_res.keys():
            if (int(pos) <= nres_first_chain):
                chain=pose.pdb_info().chain(int(pos))
                resfile_rec.write(pos+" "+chain+" USE_INPUT_SC PIKAA "+dic_res[pos]+'\n')
        resfile_rec.close()

    if Mut_on_lig:
        for pos in dic_res.keys():
            if (int(pos) > nres_first_chain):
                chain=pose.pdb_info().chain(int(pos))
                pos_renum=str( int(pos) -  nres_first_chain)
                resfile_lig.write(pos+" "+chain+" USE_INPUT_SC PIKAA "+dic_res[pos]+'\n')
        resfile_lig.close()


def toulbar2_subprocess(mut):
      if verbosity > 0:
            print("Lunch Mutation",mut)
      mut_folder=RESULT_FOLDER+mut
      if cores ==1:
          outtime_file=open(outtime_name,"a")
          outtime_file.write(mut+" ")
          outtime_file.close()                  
      if Mut_on_rec:
            start_mut_time=time.time()
            f=open(mut_folder+"/receptor.sol",'r')
            sol_rec=[int(i) for i in f.readlines()[0].split()]
            f.close()
            opt=""
            for i in range(0,len(sol_rec)):
                  if not i+1 in flexibles_rec:
                        opt+=","+str(i)+"="+str(sol_rec[i])
            if timer == 0:
                command=[toulbar2,INPUT_FOLDER+"receptor.LG",mut_folder+"/receptor.seq","-logz","-hbfs:","-epsilon="+epsilon,"-ztmp="+temp,"-x="+opt]
            if timer >0:
                command=[toulbar2,INPUT_FOLDER+"receptor.LG",mut_folder+"/receptor.seq","-timer="+str(timer),"-logz","-hbfs:","-epsilon="+epsilon,"-ztmp="+temp,"-x="+opt]
            tb2_output=check_output(command,universal_newlines=True)
            Z_output=tb2_output.split("\n")
            for line in Z_output:
                  if "<= Log(Z) <=" in line:
                        Z_rec=float(line.split()[4])
            if forced_output > 0:
                Zlog=open(mut_folder+"/receptor.Zsol",'w')
                Zlog.write(tb2_output)
                Zlog.close()
            end_mut_time=time.time()-start_mut_time
            if cores ==1:
                outtime_file=open(outtime_name,"a")
                outtime_file.write(str(end_mut_time)+" ")
                outtime_file.close()
            else:
                outtime_file=open(outtime_name,"a")
                outtime_file.write(mut+" receptor "+str(end_mut_time)+"\n")
                outtime_file.close()
                
      if Mut_on_lig:
            start_mut_time=time.time()
            flexibles_lig_renum=[i-nres_rec for i in flexibles_lig]
            f=open(mut_folder+"/ligand.sol",'r')
            sol_lig=[int(i) for i in f.readlines()[0].split()]
            f.close()
            opt=""
            for i in range(0,len(sol_lig)):
                  if not i+1 in flexibles_lig_renum:
                        opt+=","+str(i)+"="+str(sol_lig[i])

            if timer==0:
                 command=[toulbar2,INPUT_FOLDER+"ligand.LG",mut_folder+"/ligand.seq","-logz","-hbfs:","-epsilon="+epsilon,"-ztmp="+temp,"-x="+opt]
            if timer >0:
                command=[toulbar2,INPUT_FOLDER+"ligand.LG",mut_folder+"/ligand.seq","-timer="+str(timer),"-logz","-hbfs:","-epsilon="+epsilon,"-ztmp="+temp,"-x="+opt]
            tb2_output=check_output(command,universal_newlines=True)
            Z_output=tb2_output.split("\n")
            for line in Z_output:
                  if "<= Log(Z) <=" in line:
                        Z_lig=float(line.split()[4])
            if forced_output > 0 :
                Zlog=open(mut_folder+"/ligand.Zsol",'w')
                Zlog.write(tb2_output)
                Zlog.close()
            end_mut_time=time.time()-start_mut_time
            if cores==1:
                outtime_file=open(outtime_name,"a")
                outtime_file.write(str(end_mut_time)+" ")
                outtime_file.close()
            else:
                outtime_file=open(outtime_name,"a")
                outtime_file.write(mut+" ligand "+str(end_mut_time)+"\n")
                outtime_file.close()
                                                
                
      start_mut_time=time.time()
      f=open(mut_folder+"/"+mut+'.sol','r')
      sol=[int(i) for i in f.readlines()[0].split()]
      f.close()
      opt=""
      for i in range(0,len(sol)):
          if not i+1 in flexibles:
            opt+=","+str(i)+"="+str(sol[i])
      if timer==0:
          command=[toulbar2,INPUT_FOLDER+"complex.LG",mut_folder+"/"+mut+'.seq',"-logz","-hbfs:","-epsilon="+epsilon,"-ztmp="+temp,"-x="+opt]
      if timer>0:
          command=[toulbar2,INPUT_FOLDER+"complex.LG",mut_folder+"/"+mut+'.seq',"-timer="+str(timer),"-logz","-hbfs:","-epsilon="+epsilon,"-ztmp="+temp,"-x="+opt]
      if verbosity >1:
          print(command)
      tb2_output=check_output(command,universal_newlines=True)
      Z_cp_output=tb2_output.split("\n")
      for line in Z_cp_output:
            if "<= Log(Z) <=" in line:
                  Z_cp=float(line.split()[4])
      if forced_output > 0 :
          Zlog=open(mut_folder+"/"+mut+".Zsol",'w')
          Zlog.write(tb2_output)
          Zlog.close()
      print("Finished Mutation",mut)
      end_mut_time=time.time()-start_mut_time
      if cores==1:
          outtime_file=open(outtime_name,"a")
          outtime_file.write(str(end_mut_time)+"\n")
          outtime_file.close()
      else:
          outtime_file=open(outtime_name,"a")
          outtime_file.write(mut+" complex "+str(end_mut_time)+"\n")
          outtime_file.close()
      if Mut_on_lig and Mut_on_rec:
            return mut,Z_cp-Z_rec-Z_lig
      elif Mut_on_lig and not Mut_on_rec:
            return mut,Z_cp-Z_lig
      elif Mut_on_rec and not Mut_on_lig:
            return mut,Z_cp-Z_rec
        
def print_options():
    print("Doing Affinity Prediction on "+bcolors.BGREEN+pdb_file+bcolors.ENDC)
    print("Scoring function:",bcolors.BGREEN+score+bcolors.ENDC)
    print("Using",bcolors.BGREEN+str(cores)+" cores"+bcolors.ENDC)
    if float(temp) >0:
        print("Temparature",bcolors.BGREEN+temp+"C°"+bcolors.ENDC)

    if min_steps>0:
        print("Minimisation: "+bcolors.BGREEN+str(min_steps)+" step(s) with "+str(bb_cst_sd)+" stdev for harmonic cst"+bcolors.ENDC)
    else:
        print("Minimization: "+bcolors.BRED+"OFF"+bcolors.ENDC)
    if fast_relax > 0:
        print("Fast Relax: "+bcolors.BGREEN+str(fast_relax)+" step(s)"+bcolors.ENDC)
    else:
        print("Fast Relax: "+bcolors.BRED+"OFF"+bcolors.ENDC)
    if ex1 > 0:
        print("Extend Rotamer level "+bcolors.BGREEN+"1"+bcolors.ENDC)
    if ex2 > 0:
        print("Extend Rotamer level "+bcolors.BGREEN+"2"+bcolors.ENDC)
    if forced_output>0:
        print("Forced Mode: More output file produced")
        print(bcolors.BRED+"WARNING: JayZ rap slower"+bcolors.ENDC)
    if timer > 0:
        print("Time out by mutation :"+bcolors.BGREEN+hmsTime(timer)+" s"+bcolors.ENDC)
        
def rosetta_init(ex1,ex2,score,verbosity):
    initialisation="-out:level "+str(verbosity*100)

    if ex1==1:
        initialisation += " -ex1 true -ex1aro false"
    if ex2==1:
        initialisation += " -ex2 true -ex2aro false"
    if score=="beta_nov15" or score=="beta_nov15_soft":
        initialisation += " -corrections::beta_nov15"
    elif score=="beta_nov16" or score=="beta_nov16_soft":
        initialisation += " -corrections::beta_nov16"
    init(initialisation)
        
def hmsTime(seconds):
    time_string=''
    hours=0
    minutes=0
    if seconds > 3600:
        hours=math.floor(seconds/3600)
        seconds = seconds - hours*3600
    if seconds > 60:
        minutes = math.floor(seconds/60)
        seconds= seconds - minutes*60
    time_string += format(hours,'02d')+"''"
    time_string += format(minutes,'02d')+"'"
    time_string += format(seconds,'02d')
    return time_string
    
def JayZ():
      for mut in sequences:
          if len(mut)!=0:
            mut_folder=RESULT_FOLDER+mut            
            if not os.path.exists(mut_folder):
                os.mkdir(mut_folder)
                
            ###### Full Side Chain Positioning
            ##### FOR THE RECEPTOR
            if Mut_on_rec and not os.path.exists(mut_folder+"/receptor.sol"):
                extract_sequence_file(pose_prot_1,mut_folder+"/receptor.seq",mut,nres_rec,nres_lig,nres_tot) # product ".seq" file for toulbar2 sequence mask        
                if verbosity >0:
                    print("Launching toulbar2 on receptor.LG","with "+mut_folder+"/receptor.seq")
                command=[toulbar2,INPUT_FOLDER+"receptor.LG",mut_folder+"/receptor.seq","-w="+mut_folder+"/receptor.sol"]
                if float(temp) >0:
                    command.append("-ztmp="+temp)
                tb2out=check_output(command,universal_newlines=True)

            ##### FOR THE LIGAND
            if Mut_on_lig and not os.path.exists(mut_folder+"/ligand.sol"):
                extract_sequence_file(pose_prot_2,mut_folder+"/ligand.seq",mut,nres_rec,nres_lig,nres_tot) # product ".seq" file for toulbar2 sequence mask        
                if verbosity > 0:
                    print("Launching toulbar2 on ligand.LG","with "+mut_folder+"/ligand.seq")
                                
                command=[toulbar2,INPUT_FOLDER+"ligand.LG",mut_folder+"/ligand.seq","-w="+mut_folder+"/ligand.sol"]
                if float(temp) >0:
                    command.append("-ztmp="+temp)
                tb2out=check_output(command,universal_newlines=True)
                    
            #### FOR THE COMPLEX
            if not os.path.exists(mut_folder+"/"+mut+".sol"):
                extract_sequence_file(pose,mut_folder+"/"+mut+'.seq',mut,nres_rec,nres_lig,nres_tot) # product ".seq" file for toulbar2 sequence mask        
                if verbosity >0:
                    print("Launching toulbar2 on"+mut+'.LG',"with "+mut_folder+"/"+mut+'.seq')
                command=[toulbar2,INPUT_FOLDER+"complex.LG",mut_folder+"/"+mut+'.seq',"-w="+mut_folder+"/"+mut+'.sol']
                if float(temp) >0:
                    command.append("-ztmp="+temp)
                tb2out=check_output(command,universal_newlines=True)

      if __name__ == "__main__":
          results = Parallel(n_jobs=cores)(delayed(toulbar2_subprocess)(mut) for mut in sequences)
          results = sorted(results,key=itemgetter(1))
          if os.path.exists(RESULT_FOLDER+output_name+"_"+str(dist)+".K"):
              os.remove(RESULT_FOLDER+output_name+"_"+str(dist)+".K")
                
          out_file=open(RESULT_FOLDER+output_name+"_"+str(dist)+".K",'a')
          for res in results:
              out_file.write(res[0]+" "+str(res[1])+"\n")
          out_file.close()
          end_time=round(time.time()-start_time) 
          print("Finish affinity predition in:",bcolors.BGREEN+hmsTime(end_time)+bcolors.ENDC)

parser=optparse.OptionParser()
parser.add_option('--pdb', dest = 'pdb_file',
                  default = '',    
                  help = 'Protein comple in PDB format' )
parser.add_option('--seq', dest = 'seq_file',
                  default = '',    
                  help = 'Sequences to map' )
parser.add_option('--partner', dest = 'partner',
                  default = 'A_B',
                  help = 'partner chains\' name' )
parser.add_option('--temp', dest = 'temp',
                  default = str(25),
                  help = 'Temperature' )
parser.add_option('--dist', dest = 'dist',
                  default = 3.0,
                  help = 'Interface Distance' )
parser.add_option("--level",dest="level",
                  default="SC",
                  help="set the level of the contact: all = All atom / CA = alpha carbon / noH = no hydrogen/ SC=Side Chain")
parser.add_option('--eps', dest = 'epsilon',
                  default = str(1000),
                  help = 'Epsilon approximation of Z*' )
parser.add_option('--min', dest = 'mini',
                  default = 0,
                  help = 'Min' )
parser.add_option('--cst', dest='cst' ,
                  default = 0.5,
                  help = 'Move backbone during minization under constraint with stdev of [--cst=float] (default 0.5)')
parser.add_option('--relax', dest = 'relax',
                  default = 0,
                  help = 'Number (default 0) of Fast Relax' )
parser.add_option('--ex1', dest = 'ex1',
                  default = 0,
                  help = 'Options to set the 1st extended version or Dunbrack librairy' )
parser.add_option('--ex2', dest = 'ex2',
                  default = 0,
                  help = 'Options to set the 2nd extended version or Dunbrack librairy' )
parser.add_option('--score', dest = 'score',
                  default = 'beta_nov16_soft',
                  help = 'Score' )
parser.add_option('--cores', dest = 'cores',
                  default = 1,
                  help = 'number of Cores' )
parser.add_option('--v', dest = 'verbosity',
                  default = 0,
                  help = 'Verbose level' )
parser.add_option('--forced_out', dest = 'output_forced',
                  default = 0,
                  help = 'Options to get more output files' )
parser.add_option('--tb2_path', dest = 'toulbar2',
                  default = os.path.join(os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda:0))),"toulbar2"),
                  help = 'Options to set toulbar2 path' )
parser.add_option('--timer', dest='timer',
                  default = 0,
                  help= 'Time limit for each mutation in second')
(options,args) = parser.parse_args()

pdb_file=options.pdb_file
seq_file = options.seq_file
partner=options.partner
temp=options.temp
dist=float(options.dist)
epsilon=options.epsilon
min_steps=int(options.mini)
bb_cst_sd=float(options.cst)
fast_relax=int(options.relax)
ex1=int(options.ex1)
ex2=int(options.ex2)
score=options.score
cores=int(options.cores)
verbosity=int(options.verbosity)
forced_output=int(options.output_forced)
toulbar2=options.toulbar2
timer=int(options.timer)
################# MUTATION, PDB and MATRIX PRODUCTION #############
if os.path.exists( os.getcwd() + '/' + pdb_file ) and pdb_file and os.path.exists(toulbar2):
      start_time=time.time()
      print_options()
      rosetta_init(ex1,ex2,score,verbosity) ## INITIALIZE PYROSETTA
      process=0
      output_name=pdb_file.split('.pdb')[0]
      RESULT_FOLDER=os.getcwd()+"/"+output_name+"_"+score
      if min_steps > 0:
          RESULT_FOLDER= RESULT_FOLDER+"_min"
      if ex1==1:
          RESULT_FOLDER= RESULT_FOLDER + "_ex1"
      if ex2==1:
          RESULT_FOLDER = RESULT_FOLDER + "_ex2"
          
      RESULT_FOLDER += '/'
      if not os.path.exists(RESULT_FOLDER):
          os.mkdir(RESULT_FOLDER)      
      INPUT_FOLDER = RESULT_FOLDER+'input/'
      if not os.path.exists(INPUT_FOLDER):
          os.mkdir(INPUT_FOLDER)
          
      if  options.seq_file == '':
            seq_file = pdb_file.split(".pdb")[0]+".seq"
      else:
            seq_file = options.seq_file

      outtime_name=RESULT_FOLDER+output_name+"_"+str(dist)+"_Z.time"
      if os.path.exists(outtime_name):
          os.remove(outtime_name)

      pdb = PDBParser().get_structure(output_name,pdb_file)
      chain_name=partner.split('_')
      Separate_complex(pdb,chain_name,INPUT_FOLDER) ## Split partners
      
      num_cores = multiprocessing.cpu_count()
      if cores > num_cores:
            core=num_cores

      pose_prot_1=Pose()
      pose_prot_1=pose_from_file(INPUT_FOLDER+pdb.get_id() + "_" + chain_name[0] + ".pdb")
      nres_rec=int(pose_prot_1.pdb_info().nres())
    
      pose_prot_2=Pose()
      pose_prot_2=pose_from_file(INPUT_FOLDER+pdb.get_id() + "_" + chain_name[1] + ".pdb")
      nres_lig=int(pose_prot_2.pdb_info().nres())
    
      pose=Pose()
      pose=pose_from_file(pdb_file)
      nres_tot=int(pose.pdb_info().nres())

      if forced_output>0: ## if wanted throw out the complex with hydrogens
          pose.dump_pdb(INPUT_FOLDER+output_name+"_h.pdb")
    
      ## parse the mutation sequence file into a list: sequences=[list of mutation]
      sequences=[]
      numero_mut_lig=[]
      numero_mut_rec=[]
      Mut_on_rec=False
      Mut_on_lig=False
      if os.path.exists( os.getcwd() + '/' + seq_file ) and seq_file:
            r = re.compile("([0-9]+)([a-zA-Z]+)")
            seqfile=open(seq_file,'r')
            for seq in seqfile.read().split('\n'):
                  if len(seq) > 0 :
                        name_pos=r.match(seq).groups()
                        if int(name_pos[0]) <= nres_rec:
                              Mut_on_rec=True
                              numero_mut_rec.append(int(name_pos[0]))
                        if int(name_pos[0]) > nres_rec:
                              Mut_on_lig=True
                              numero_mut_lig.append(int(name_pos[0]))
                        sequences.append(seq)
            sequences.append(pdb_file.split('.pdb')[0])
      else:
            print(bcolors.BRED+"ERROR: "+  os.getcwd() + '/' + seq_file+" file not existing, exit..."+bcolors.ENDC)
            exit(0)
      l=len(sequences)
      if verbosity > 0:
            print(sequences)
      if Mut_on_rec==False and Mut_on_lig==False:
            print(bcolors.BRED+"Warning: Mutation on any of chains"+bcolors.ENDC)
            exit(0)
      elif Mut_on_rec ==False and Mut_on_lig==True:
            print(bcolors.BBLUE+"No mutation on chain "+chain_name[0]+bcolors.ENDC)
      elif Mut_on_rec ==True and Mut_on_lig==False:
            print(bcolors.BBLUE+"No mutation on chain "+chain_name[1]+bcolors.ENDC)
      else:
            print(bcolors.BBLUE+"Found mutation on both chain"+bcolors.ENDC)

      if min_steps>0:
        print(bcolors.BBLUE+"Minimizing..."+bcolors.ENDC)
        if score=="beta_nov16" or score=="beta_nov16_soft":
            pose.assign(minimized(pose,"beta_nov16",min_steps,bb_cst_sd)) # Minimized entry pdb
            if Mut_on_rec==True:
                pose_prot_1.assign(minimized(pose_prot_1,"beta_nov16",min_steps,bb_cst_sd))
            if Mut_on_lig==True:
                pose_prot_2.assign(minimized(pose_prot_2,"beta_nov16",min_steps,bb_cst_sd))
        elif score=="beta_nov15" or score=="beta_nov15_soft":
            pose.assign(minimized(pose,"beta_nov15",min_steps,bb_cst_sd)) # Minimized entry pdb
            if Mut_on_rec==True:
                pose_prot_1.assign(minimized(pose_prot_1,"beta_nov15",min_steps,bb_cst_sd))
            if Mut_on_lig==True:
                pose_prot_2.assign(minimized(pose_prot_2,"beta_nov15",min_steps,bb_cst_sd))
        else:
            pose.assign(minimized(pose,"talaris2014",min_steps,bb_cst_sd)) # Minimized entry pdb
            if Mut_on_rec==True:
                pose_prot_1.assign(minimized(pose_prot_1,"talaris2014",min_steps,bb_cst_sd))
            if Mut_on_lig==True:
                pose_prot_2.assign(minimized(pose_prot_2,"talaris2014",min_steps,bb_cst_sd))
        print(bcolors.BBLUE+"Finish Minimized !"+bcolors.ENDC)
        if forced_output > 0 :
            pose.dump_pdb(INPUT_FOLDER+output_name+"_h_min.pdb")

      scorefxn=create_score_function(score)
      scorefxn.set_weight(ref,0)

      ## create resfile for pyrosetta
      create_resfile(pose,pose_prot_1,pose_prot_2,sequences,Mut_on_rec,Mut_on_lig,INPUT_FOLDER) 

      #### Interaction Graph for the complex if not existing
      if not os.path.exists(INPUT_FOLDER+'complex.LG') :
        print(bcolors.BBLUE+"Computing interaction graph for "+pdb_file+bcolors.ENDC)
        extract_interactions(pose,INPUT_FOLDER+pdb.get_id()+'.resfile',INPUT_FOLDER+'complex',scorefxn)

      ### Interaction Graph for the receptor (if any mutation on it)
      if Mut_on_rec and not os.path.exists(INPUT_FOLDER+'receptor.LG'):
        print(bcolors.BBLUE+"Computing interaction graph for chain "+chain_name[0]+bcolors.ENDC)
        extract_interactions(pose_prot_1,INPUT_FOLDER+pdb.get_id() + "_" + chain_name[0]+'.resfile', INPUT_FOLDER+'receptor',scorefxn)

      ### Interaction Graph for the ligand (if any mutation on it)
      if Mut_on_lig and not os.path.exists(INPUT_FOLDER+"ligand.LG"):
        print(bcolors.BBLUE+"Computing interaction graph for chain "+chain_name[1]+bcolors.ENDC)
        extract_interactions(pose_prot_2,  INPUT_FOLDER+pdb.get_id() + "_" + chain_name[1]+'.resfile',INPUT_FOLDER+'ligand',scorefxn)

      if cores == 1:
          outtime_file=open(outtime_name,"a")
          outtime_file.write("System ")
          if Mut_on_rec:
              outtime_file.write("receptor ")
          if Mut_on_lig:
              outtime_file.write("ligand ")
          outtime_file.write("complex\n")
          outtime_file.close()
              
                    
      #residue list
      res_mut=numero_mut_rec+numero_mut_lig
      res_mut_final=[]
      res_protein_list = Selection.unfold_entities(pdb, 'R')
      for res in res_protein_list:
          for res_2 in res_mut:
              if res.get_id()[1] == int(res_2):
                  res_mut_final.append(res)


      chain_name=[]
      chain_length=[]
      for chain in pdb.get_chains():
          chain_length.append(len(chain))
          chain_name.append(chain.get_id())

      atom_list=[]
      if options.level=="all":
          atom_list = Selection.unfold_entities(pdb, 'A') # A for atom
      elif options.level=="CA":
          res_list = Selection.unfold_entities(pdb, 'R') # R for atom residue
          for res in res_list:
              atom_list.append(res["CA"])
      elif options.level=="SC":
          atom_list = Selection.unfold_entities(pdb, 'A') # A for atom
          noH_atom_list=[]
          for atom in atom_list:
              if not atom.element=='H' and not atom.get_name() == 'CA' and not atom.get_name() == 'C' and not atom.get_name() == 'O' and not atom.get_name() == 'N' :
                  noH_atom_list.append(atom)
          atom_list=noH_atom_list
    
      elif options.level=="noH":
          atom_list = Selection.unfold_entities(pdb, 'A') # A for atom
          noH_atom_list=[]
          for atom in atom_list:
              if not atom.element=='H':
                  noH_atom_list.append(atom)
          atom_list=noH_atom_list

      ns = NeighborSearch(atom_list)
      contact=[]

      for residue_1 in res_mut_final:
          atom_list=[]
          for center in residue_1:
              center=center.get_coord()
              neighbors = ns.search(center, dist,level='R') # cutoff for distance in angstrom
              contact.append(neighbors)

      contact_id=[]
      for res_vect in contact:
          for res_full_id in res_vect:
              contact_id.append(res_full_id.get_id()[1])
      flexibles=sorted(list(set(contact_id+res_mut)))
      if verbosity>0:
          print("Flexible residues are:",flexibles)
      flexibles_rec=[]
      flexibles_lig=[]
      for i in flexibles:
          if i <= nres_rec:
              flexibles_rec.append(i)
          else:
              flexibles_lig.append(i)
              
      JayZ()
      end_time=round(time.time()-start_time)
      print("\nTerminating:", hmsTime(end_time))
      outtime_file=open(outtime_name,"a")
      outtime_file.write("Total "+str(end_time))
      outtime_file.close()
elif not os.path.exists(os.getcwd() + '/'+ pdb_file):
    print(bcolors.BRED+"ERROR: PDB file not existing, exit..."+bcolors.ENDC)
elif not os.path.exists(toulbar2):
    print(bcolors.BRED+"ERROR: toulbar2 not existing, exit..."+bcolors.ENDC)
    
