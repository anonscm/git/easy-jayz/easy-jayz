#!/usr/bin/env python3
import time
import math
import os
import optparse
import re
import subprocess
from subprocess import *
import numpy as np
from operator import *
import inspect

import Bio
from Bio import *
from Bio.PDB import *

import pyrosetta
from pyrosetta.rosetta import *
from pyrosetta.rosetta.core.scoring import *
from pyrosetta.rosetta.core.scoring.constraints import *
from pyrosetta.rosetta.core.pack.rotamer_set import *
from pyrosetta.rosetta.core.pack.interaction_graph import *
from pyrosetta.rosetta.core.pack import *
from pyrosetta.rosetta.protocols.simple_moves import * 
from pyrosetta.rosetta.protocols.relax import *
from pyrosetta.rosetta.core.pack.task import *
from pyrosetta.rosetta.protocols.minimization_packing import MinMover

from pyrosetta import *
from pyrosetta.toolbox import *

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

class bcolors:
    BRED = '\x1b[1;31;40m'
    BBLUE = '\x1b[1;34;40m'
    BGREEN = '\x1b[1;32;40m'
    ENDC = '\x1b[0m'
    BOLD = '\x1b[1m'
    UNDERLINE = '\x1b[4m'
    
one_to_three = {'A': 'ALA',
                'R': 'ARG',
                'N': 'ASN',
                'D': 'ASP',
                'C': 'CYS',
                'E': 'GLU',
                'Q': 'GLN',
                'G': 'GLY',
                'H': 'HIS',
                'I': 'ILE',
                'L': 'LEU',
                'K': 'LYS',
                'M': 'MET',
                'F': 'PHE',
                'P': 'PRO',
                'S': 'SER',
                'T': 'THR',
                'W': 'TRP',
                'Y': 'TYR',
                'V': 'VAL',
            }

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', time=-1,decimals = 1, length = 100, fill = bcolors.BRED+'█'+bcolors.ENDC):
        
    # Call in a loop to create terminal progress bar
    # @params:
    #     iteration   - Required  : current iteration (Int)
    #     total       - Required  : total iterations (Int)
    #     prefix      - Optional  : prefix string (Str)
    #     decimals    - Optional  : positive number of decimals in percent complete (Int)
    #     length      - Optional  : character length of bar (Int)
    #     fill        - Optional  : bar fill character (Str)

    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength +(bcolors.BGREEN+'-'+bcolors.ENDC) * (length - filledLength)
    if time < 0:
        print('\r'+prefix+' '+percent+'% |'+bar+'|' , end='\r')
    else:
        print('\r'+prefix+' '+percent+'% |'+bar+'| ', end = "Remaining approximatively "+hmsTime(time)+'\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def hmsTime(seconds):
    time_string=''
    hours=0
    minutes=0
    if seconds > 3600:
        hours=math.floor(seconds/3600)
        seconds = seconds - hours*3600
    if seconds > 60:
        minutes = math.floor(seconds/60)
        seconds= seconds - minutes*60
    time_string += format(hours,'02d')+":"
    time_string += format(minutes,'02d')+"'"
    time_string += format(seconds,'02d')+'"'
    return time_string
    
def minimized(pose,score,min_steps,bb_cst_sd):
    copy_pose = Pose()
    copy_pose.assign(pose)

    movemap = MoveMap()
    movemap.set_chi(True)
    best_pose=Pose()
    best_pose.assign(copy_pose)
    score_fxn=create_score_function(score)
        
    if bb_cst_sd>0:
        movemap.set_bb(True) ## Move Backbone
        add_coordinate_constraints(copy_pose, bb_cst_sd) #coordinate constraint with distance dist.
        score_fxn.set_weight(coordinate_constraint,1) #set the coordinate constraint weight to 1        
    else:
        movemap.set_bb(False)
        
    tolerance = 0.001 ## Set the tolerance, minimum will be in tolerance of a local min
    min_type = "dfpmin" ## Quasi Newton
    minmover = MinMover(movemap, score_fxn, min_type, tolerance, True)
    for i in range(0,min_steps):
        if bb_cst_sd>0 and verbosity>0:
            print("Doing "+str(i+1)+"th side-chain and backbone minimization with "+score)
        elif bb_cst_sd == 0 and verbosity>0:
            print( "Doing "+str(i+1)+"th side-chain minimization with "+score)
        minmover.apply(copy_pose)
        if verbosity > 1 :
            print("Best pose:",score_fxn(best_pose),"Actual min pose:",score_fxn(copy_pose))
        if score_fxn(copy_pose) < score_fxn(best_pose):
            best_pose.assign(copy_pose)
    return best_pose


def fast_relaxed(pose,score_fxn,fast_relax):
    relax=FastRelax(score_fxn)
    for i in range(0,fast_relax):
        if verbosity > 0:
            print("Doing "+str(i+1)+"th Fast relax")
        relax.apply(pose)
    return pose


def extract_sequence_file(pose,out,mut,nres_rec,nres_lig,nres_tot):
    r = re.compile("([0-9]+)([a-zA-Z]+)")
    target_sequence=pose.sequence()
    if verbosity>1:
        print(target_sequence)
    if not mut == pdb_file.split('.pdb')[0]:
        for mutation in mut.split('_'):
            name_pos_mutAA=r.match(mutation).groups()
            if len(target_sequence) == nres_tot:
                target_sequence=target_sequence[:int(name_pos_mutAA[0])-1]+name_pos_mutAA[1]+target_sequence[int(name_pos_mutAA[0]):]
            if len(target_sequence) == nres_rec and int(name_pos_mutAA[0]) <= nres_rec:
                target_sequence=target_sequence[:int(name_pos_mutAA[0])-1]+name_pos_mutAA[1]+target_sequence[int(name_pos_mutAA[0]):]
            if len(target_sequence) == nres_lig and nres_rec < int(name_pos_mutAA[0]):
                pos_renum=int(name_pos_mutAA[0])-nres_rec
                target_sequence=target_sequence[:pos_renum-1]+name_pos_mutAA[1]+target_sequence[pos_renum:]
    outfile=open(out,'w')
    outfile.write(target_sequence)
    outfile.close()
               
def extract_interactions(pose,resfile,out,scorefxn):
    copy_pose=Pose()
    copy_pose.assign(pose)
    task_design = TaskFactory.create_packer_task(copy_pose)
    task_design.initialize_from_command_line()
#    with open(resfile,'r') as file:
#        lines = file.readlines()
#        for line in lines:
#            print(line)
    parse_resfile(copy_pose, task_design, resfile)
    copy_pose.update_residue_neighbors()
    png = create_packer_graph(copy_pose, scorefxn, task_design)
    rotsets = RotamerSets()
    pack_rotamers_setup(copy_pose, scorefxn, task_design, rotsets)
    ig = InteractionGraphFactory.create_and_initialize_two_body_interaction_graph(task_design, rotsets, copy_pose, scorefxn, png)
    if verbosity > 1:
        print("Design:",task_design.design_any())
        print("Residue design vector:")
        print(task_design.designing_residues())
             
    out_LG=out+'.LG'
    out_uai=out+'.uai'
    g = open(out_LG,'w')
    h = open(out_uai,'w')
    g.write("MARKOV\n")
    g.write(str(ig.get_num_nodes())+'\n')
    h.write("MARKOV\n")
    h.write(str(ig.get_num_nodes())+'\n')
    domain=StringIO()
    scope=StringIO()
    unary_terms=StringIO()
    exp_unary_terms=StringIO()
    binary_terms=StringIO()
    exp_binary_terms=StringIO()
    seq_AA_rot=StringIO()
    number_of_functions=0
    for res1 in range(1,ig.get_num_nodes()+1):
        number_of_functions += 1
        domain_res=0
        unary=[]
        exp_unary=[]
        scope.write("1 "+str(res1-1)+'\n')
        for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
            resname = rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name1()
            seq_AA_rot.write(resname)
            ener=ig.get_one_body_energy_for_node_state(res1,i)
            unary.append(str(-ener))  # TS le LG, on met oppose du log
            exp_unary.append(str(np.exp(-ener)))
            domain_res = domain_res + 1
        seq_AA_rot.write("\n")
        unary_terms.write(str(domain_res)+'\n')
        unary_terms.write(" ".join(unary)+'\n')
        exp_unary_terms.write(str(domain_res)+'\n')
        exp_unary_terms.write(" ".join(exp_unary)+'\n')
        domain.write(str(domain_res)+' ')
    domain.write('\n')

    for res1 in range(1,ig.get_num_nodes()+1):
        for res2 in range(res1+1,ig.get_num_nodes()+1):
            if (ig.get_edge_exists(res1, res2)):
                number_of_functions += 1
                scope.write("2 "+str(res1-1)+" "+str(res2-1)+"\n")
                domain_res=0
                binary=[]
                exp_binary=[]
                for i in range(1, rotsets.rotamer_set_for_moltenresidue(res1).num_rotamers()+1):
                    resname1 = rotsets.rotamer_set_for_moltenresidue(res1).rotamer(i).name1()
                    resnum1 = rotsets.rotamer_set_for_moltenresidue(res1).resid()
                    for j in range(1, rotsets.rotamer_set_for_moltenresidue(res2).num_rotamers()+1):
                        resname2 = rotsets.rotamer_set_for_moltenresidue(res2).rotamer(j).name1()
                        resnum2 = rotsets.rotamer_set_for_moltenresidue(res2).resid()
                        ener=ig.get_two_body_energy_for_edge(res1,res2,i,j)
                        binary.append(str(-ener))
                        exp_binary.append(str(np.exp(-ener)))
                        domain_res=domain_res+1
                    binary.append('\n')
                    exp_binary.append('\n')

                binary_terms.write(str(domain_res)+'\n')
                exp_binary_terms.write(str(domain_res)+'\n')
                for i in range(0,len(binary)) :
                    if i != len(binary)-1 and binary[i] != '\n':
                        binary_terms.write(binary[i]+" ")
                        exp_binary_terms.write(exp_binary[i]+" ")
                    elif binary[i] == '\n':
                        binary_terms.write(binary[i])
                        exp_binary_terms.write(exp_binary[i])

    g.write(domain.getvalue())
    h.write(domain.getvalue())
    g.write(str(number_of_functions)+'\n')
    h.write(str(number_of_functions)+'\n')
    g.write(scope.getvalue())
    h.write(scope.getvalue())
    
    g.write(unary_terms.getvalue())
    g.write(binary_terms.getvalue())

    h.write(exp_unary_terms.getvalue())
    h.write(exp_binary_terms.getvalue())

    g.write(seq_AA_rot.getvalue())
    h.write(seq_AA_rot.getvalue())

    domain.close()
    scope.close()
    unary_terms.close()
    binary_terms.close()
    exp_unary_terms.close()
    exp_binary_terms.close()
    g.close()
    h.close()


def load_assign(pose, assign, resfile,score_fxn):
    copy_pose=Pose()
    copy_pose.assign(pose)
    task_design = TaskFactory.create_packer_task(pose)
    task_design.initialize_from_command_line()
    parse_resfile(pose, task_design, resfile)
    copy_pose.update_residue_neighbors()
    png = create_packer_graph(copy_pose, score_fxn, task_design)
    rotsets = RotamerSets()
    ig = pack_rotamers_setup(copy_pose, score_fxn, task_design, rotsets)
    mat = np.loadtxt(assign, dtype=int)
    for i in range(0, len(mat)):
        res = rotsets.rotamer_set_for_moltenresidue(i+1).rotamer(int(mat[i]+1))
        copy_pose.replace_residue(rotsets.moltenres_2_resid(i+1), res, False)
    return copy_pose

def rosetta_init():
    initialisation="-out:level "+str(verbosity*100)

    if ex1==1:
        initialisation += " -ex1 true -ex1aro false"
    if ex2==1:
        initialisation += " -ex2 true -ex2aro false"
    if score=="beta_nov15" or score=="beta_nov15_soft":
        initialisation += " -corrections::beta_nov15"
    elif score=="beta_nov16" or score=="beta_nov16_soft":
        initialisation += " -corrections::beta_nov16"
    if int(options.ignore_zero_occupancy)>0:
        initialisation += " -ignore_zero_occupancy false"
    init(initialisation)
    
def eshow(scorefunction, pose, filen):
    copy_pose=Pose()
    copy_pose.assign(pose)
    with open(filen,'w') as f:
        score = scorefunction(copy_pose)
        f.write("------------------------------------------------------------\n")
        f.write(" Scores                       Weight   Raw Score Wghtd.Score\n")
        f.write("------------------------------------------------------------\n")
        sum_weighted = 0.0
        w = scorefunction.weights()
        te = copy_pose.energies().total_energies()
        nzwt =  scorefunction.get_nonzero_weighted_scoretypes()
        for ti in range(1,len(nzwt)+1):
            name = name_from_score_type(nzwt[ti])
            weight = w[nzwt[ti]]
            energy = te[nzwt[ti]]
            wenergy = weight*energy
            f.write("{:24s}".format(name)+' ')
            f.write("{:9.3f}".format(weight)+'   ')
            f.write("{:9.3f}".format(energy)+'   ')
            f.write("{:9.3f}".format(wenergy)+'\n')
            sum_weighted += te[nzwt[ti]]*w[nzwt[ti]]
        f.write("---------------------------------------------------\n")
        f.write(" Total weighted score:                           {:9.3f}".format(sum_weighted)+'\n')

def print_options():
    print("Doing affinity prediction on "+bcolors.BGREEN+pdb_file+bcolors.ENDC)
    print("Scoring function:",bcolors.BGREEN+score+bcolors.ENDC)
    if float(temp)>0:
        print("Temperature",bcolors.BGREEN+temp+"C°"+bcolors.ENDC)
    if min_steps > 0:
        print("Minimisation: "+bcolors.BGREEN+str(min_steps)+" step(s) with "+str(bb_cst_sd)+" stdev for harmonic cst"+bcolors.ENDC)
    else:
        print("Minimisation: "+bcolors.BRED+"OFF"+bcolors.ENDC)

    if fast_relax > 0:
        print("Fast Relax: "+bcolors.BGREEN+str(fast_relax)+" step(s)"+bcolors.ENDC)
    else:
        print("Fast Relax: "+bcolors.BRED+"OFF"+bcolors.ENDC)

    if ex1 > 0:
        print("Extend rotamer level "+bcolors.BGREEN+"1"+bcolors.ENDC)
    if ex2 > 0:
        print("Extend rotamer level "+bcolors.BGREEN+"2"+bcolors.ENDC)
    if forced_mut_on_rec>0:
        print("Force computation of receptor\'s matrix: "+bcolors.BGREEN+"ON"+bcolors.ENDC)
    if forced_mut_on_lig>0:
        print("Force computation of ligand\'s matrix: "+bcolors.BGREEN+"ON"+bcolors.ENDC)
    if forced_output>0:
        print("Forced Mode: More output file produced")
        print(bcolors.BRED+"WARNING: EasyE rap slower"+bcolors.ENDC)
    if int(options.ignore_zero_occupancy)>0:
        print("Ignore Zero Occupancy : "+bcolors.BGREEN+"ON"+bcolors.ENDC)
    
def Separate_complex(pdb,chain_name,folder):
    pdbio = PDBIO()
    pdbio.set_structure(pdb)
    receptor_chain=list(chain_name[0])
    ligand_chain=list(chain_name[1])
    
    class Receptor_selector(Select):
        def accept_chain(self,chain):
            if chain.get_id() in receptor_chain:
                return 1
            else:
                return 0

    class Ligand_selector(Select):
        def accept_chain(self,chain):
            if chain.get_id() in ligand_chain:
                return 1
            else:
                return 0
    pdbio.save(folder+output_name + "_" + chain_name[0]+ ".pdb",Receptor_selector())
    pdbio.save(folder+output_name + "_" + chain_name[1]+ ".pdb",Ligand_selector())

    if os.stat(folder+output_name + "_" + chain_name[0]+ ".pdb").st_size==0:
        print(bcolors.BRED+'ERROR the file '+folder+output_name + "_" + chain_name[0]+ ".pdb is empty"+bcolors.ENDC)
        print(bcolors.BRED+'Check if the partner are set right'+bcoors.ENDC)
        print(bcolors.BRED+'Exit....')
        exit(0)
    elif os.stat(folder+output_name + "_" + chain_name[1]+ ".pdb").st_size==0:
        print(bcolors.BRED+'ERROR the file '+folder+output_name + "_" + chain_name[1]+ ".pdb is empty"+bcolors.ENDC)
        print(bcolors.BRED+'Check if the partner are set right'+bcoors.ENDC)
        print(bcolors.BRED+'Exit....')
        exit(0)
                                                        
def create_resfile(pose,pose_rec,pose_lig,sequences,Mut_on_rec,Mut_on_lig,folder):
#    print(sequences)
    copy_pose=Pose()
    copy_pose.assign(pose)
    
    target_sequence=copy_pose.sequence()
    
    nres_first_chain=int(pose_rec.pdb_info().nres())
    resfile=open(folder+output_name+'.resfile','w')
    resfile.write("NATAA\n")
    resfile.write("USE_INPUT_SC\n")
    resfile.write("start\n")

    if Mut_on_rec:
        resfile_rec=open(folder+'receptor.resfile','w')
        resfile_rec.write("NATAA\n")
        resfile_rec.write("USE_INPUT_SC\n")
        resfile_rec.write("start\n")
    if Mut_on_lig:
        resfile_lig=open(folder+'ligand.resfile','w')
        resfile_lig.write("NATAA\n")
        resfile_lig.write("USE_INPUT_SC\n")
        resfile_lig.write("start\n")

    dic_res={}
    r = re.compile("([0-9]+)([a-zA-Z]+)")
    for mut in sequences:
        if not mut == pdb_file.split('.pdb')[0]:
            for mutation in mut.split('_'):
                name_pos=r.match(mutation).groups()
                if (name_pos[0] in dic_res.keys()):
                    if (dic_res[name_pos[0]].find(name_pos[1]) == -1):
                        dic_res[name_pos[0]] += name_pos[1]
                else:
                    dic_res[name_pos[0]] = name_pos[1]
    
    for pos in dic_res.keys():
        dic_res[pos] += target_sequence[int(pos)-1]
        chain = pose.pdb_info().chain(int(pos))
        resfile.write(pos+" "+chain+" USE_INPUT_SC PIKAA "+dic_res[pos]+'\n')
    resfile.close()

    if Mut_on_rec:
        for pos in dic_res.keys():
            if (int(pos) <= nres_first_chain):
                chain=pose.pdb_info().chain(int(pos))
                resfile_rec.write(pos+" "+chain+" USE_INPUT_SC PIKAA "+dic_res[pos]+'\n')
        resfile_rec.close()

    if Mut_on_lig:
        for pos in dic_res.keys():
            if (int(pos) > nres_first_chain):
                chain=pose.pdb_info().chain(int(pos))
                pos_renum=str( int(pos) -  nres_first_chain)
                resfile_lig.write(pos+" "+chain+" USE_INPUT_SC PIKAA "+dic_res[pos]+'\n')
        resfile_lig.close()
     
def EasyE():

    if not os.path.exists(RESULT_FOLDER+"input"):
        os.mkdir(RESULT_FOLDER+"input")
    INPUT_FOLDER=RESULT_FOLDER+"input/"

    ## Create scoring function 
    scorefxn = create_score_function(score)
    scorefxn.set_weight(ref,0) ## set the reference energy to zero
    
    pdb = PDBParser().get_structure(output_name,pdb_file)
    chain_name=partner.split('_')
    Separate_complex(pdb,chain_name,INPUT_FOLDER) ## split the partner into to different pdb

    ## Load the first partner
    pose_prot_1=Pose()
    pose_prot_1=pose_from_file(INPUT_FOLDER+output_name + "_" + chain_name[0] + ".pdb")
    nres_rec=int(pose_prot_1.pdb_info().nres())

    ## Load the second partner
    pose_prot_2=Pose()
    pose_prot_2=pose_from_file(INPUT_FOLDER+output_name + "_" + chain_name[1] + ".pdb")
    nres_lig=int(pose_prot_2.pdb_info().nres())

    ## Load the complex
    pose=Pose()
    pose=pose_from_file(pdb_file)
    nres_tot=int(pose.pdb_info().nres())

    if forced_output>0: ## if wanted throw out the complex with hydrogens
        pose.dump_pdb(INPUT_FOLDER+output_name+"_h.pdb")


    ## parse sequences file into a list: sequences=[list of mutation]
    sequences=[]
    Mut_on_rec=False ## Is there any mutation on first partner ?
    Mut_on_lig=False ## Is there any mutation on second partner ?
    if os.path.exists( os.getcwd() + '/' + seq_file ) and seq_file:
        r = re.compile("([0-9]+)([a-zA-Z]+)")
        seqfile=open(seq_file,'r')
        for seq in seqfile.read().split('\n'):
            if len(seq) > 0 :
                name_pos=r.match(seq).groups()
                if int(name_pos[0]) <= nres_rec and not Mut_on_rec:
                    Mut_on_rec=True
                if int(name_pos[0]) > nres_rec and not Mut_on_lig:
                    Mut_on_lig=True
                sequences.append(seq)
        sequences.append(pdb_file.split('.pdb')[0])
        if verbosity > 0:        
            print(sequences)
    else:
        print(bcolors.BRED+"ERROR: "+  os.getcwd() + '/' + seq_file+" file not existing, exit..."+bcolors.ENDC)
        exit(0)
    if forced_mut_on_rec > 0:
        Mut_on_rec=True
    if forced_mut_on_lig > 0:
        Mut_on_lig=True
    
    if Mut_on_rec==False and Mut_on_lig==False:
        print(bcolors.BRED+"Warning: Mutation on any of chains"+bcolors.ENDC)
        exit(0)
    elif Mut_on_rec ==False and Mut_on_lig==True:
        print(bcolors.BBLUE+"No mutation on chain "+chain_name[0]+bcolors.ENDC)
    elif Mut_on_rec ==True and Mut_on_lig==False:
        print(bcolors.BBLUE+"No mutation on chain "+chain_name[1]+bcolors.ENDC)
    else:
        print(bcolors.BBLUE+"Found mutation on both chain"+bcolors.ENDC)

    DeltaE=[]
    out_deltaE=open(RESULT_FOLDER+output_name+".DeltaE",'w')

    if forced_output>0:
        out_E=open(RESULT_FOLDER+output_name+".E",'w')
        out_E.write("Sequence Receptor Ligand Complex\n")
        
    if min_steps>0 and not os.path.exists(INPUT_FOLDER+output_name+"_h_min.pdb"):
        print(bcolors.BBLUE+"Minimizing..."+bcolors.ENDC)
        if score=="beta_nov16" or score=="beta_nov16_soft":
            pose.assign(minimized(pose,"beta_nov16",min_steps,bb_cst_sd)) # Minimized entry pdb
            if Mut_on_rec==True:
                pose_prot_1.assign(minimized(pose_prot_1,"beta_nov16",min_steps,bb_cst_sd))
            if Mut_on_lig==True:
                pose_prot_2.assign(minimized(pose_prot_2,"beta_nov16",min_steps,bb_cst_sd))
        elif score=="beta_nov15" or score=="beta_nov15_soft":
            pose.assign(minimized(pose,"beta_nov15",min_steps,bb_cst_sd)) # Minimized entry pdb
            if Mut_on_rec==True:
                pose_prot_1.assign(minimized(pose_prot_1,"beta_nov15",min_steps,bb_cst_sd))
            if Mut_on_lig==True:
                pose_prot_2.assign(minimized(pose_prot_2,"beta_nov15",min_steps,bb_cst_sd))
        else:
            pose.assign(minimized(pose,"talaris2014",min_steps,bb_cst_sd)) # Minimized entry pdb
            if Mut_on_rec==True:
                pose_prot_1.assign(minimized(pose_prot_1,"talaris2014",min_steps,bb_cst_sd))
            if Mut_on_lig==True:
                pose_prot_2.assign(minimized(pose_prot_2,"talaris2014",min_steps,bb_cst_sd))      
        print(bcolors.BBLUE+"Finish Minimized !"+bcolors.ENDC)
        pose.dump_pdb(INPUT_FOLDER+output_name+"_h_min.pdb")
        if Mut_on_rec==True:
            pose_prot_1.dump_pdb(INPUT_FOLDER+output_name + "_" + chain_name[0] +"_h_min.pdb")
        if Mut_on_lig==True:
            pose_prot_2.dump_pdb(INPUT_FOLDER+output_name + "_" + chain_name[1] +"_h_min.pdb")

    ## create resfile for pyrosetta
    create_resfile(pose,pose_prot_1,pose_prot_2,sequences,Mut_on_rec,Mut_on_lig,INPUT_FOLDER) 

    #### Interaction Graph for the complex
    if not os.path.exists(INPUT_FOLDER+'complex.LG') :
        print(bcolors.BBLUE+"Computing interaction graph for "+pdb_file+bcolors.ENDC)
        extract_interactions(pose,INPUT_FOLDER+output_name+'.resfile',INPUT_FOLDER+'complex',scorefxn)

    ### Interaction Graph for the receptor (if any mutation on it)
    if Mut_on_rec and not os.path.exists(INPUT_FOLDER+'receptor.LG'):
        print(bcolors.BBLUE+"Computing interaction graph for chain "+chain_name[0]+bcolors.ENDC)
        extract_interactions(pose_prot_1,INPUT_FOLDER+'receptor.resfile', INPUT_FOLDER+'receptor',scorefxn)

    ### Interaction Graph for the ligand (if any mutation on it)
    if Mut_on_lig and not os.path.exists(INPUT_FOLDER+"ligand.LG"):
        print(bcolors.BBLUE+"Computing interaction graph for chain "+chain_name[1]+bcolors.ENDC)
        extract_interactions(pose_prot_2,  INPUT_FOLDER+'ligand.resfile',INPUT_FOLDER+'ligand',scorefxn)

    outtime_file=open(outtime_name,"a")
    outtime_file.write("System ")
    if Mut_on_rec:
        outtime_file.write("Receptor ")
    if Mut_on_lig:
        outtime_file.write("Ligand ")
    outtime_file.write("Complex\n")
    outtime_file.close()                        
    start_mut_time=0
    end_mut_time=0
    remaining_time=-1
    l=len(sequences)
    progres=0
    if verbosity ==0:
        if Mut_on_lig == False or Mut_on_rec==False:
            printProgressBar(progres, l*2, prefix = 'Progress:', time=remaining_time, length = 50)
        else:
            printProgressBar(progres, l*3, prefix = 'Progress:', time=remaining_time,length = 50)
    ###### Mutation Loop ####
    for mut in sequences:
        if len(mut)!=0:
            outtime_file=open(outtime_name,"a")
            outtime_file.write(mut)
            outtime_file.close()
            if verbosity > 0:
                print("Processing Mutation:",mut)
            start_mut_time=time.time()
            mut_folder=RESULT_FOLDER+mut
            
            if not os.path.exists(mut_folder):
                os.mkdir(mut_folder)
            if forced_output>0:                 
                out_E.write(mut)
                
            ###### Compute matrix, the optimal solution and optimal energy.
            ##### FOR THE RECEPTOR
            if Mut_on_rec:
                extract_sequence_file(pose_prot_1,mut_folder+"/receptor.seq",mut,nres_rec,nres_lig,nres_tot) # product ".seq" file for toulbar2 sequence mask        
                if verbosity >0:
                    print("Launching toulbar2 on receptor.LG","with "+mut_folder+"/receptor.seq")
                command=[toulbar2,INPUT_FOLDER+"receptor.LG",mut_folder+"/receptor.seq","-w="+mut_folder+"/receptor.sol"]
                if float(temp) >0:
                    command.append("-ztmp="+temp)
                if verbosity > 1:
                    print(command)
                tb2out=check_output(command,universal_newlines=True)
                if forced_output>0:
                    tb2_output_file=open(mut_folder+'/receptor.tb2','w')
                    tb2_output_file.write(tb2out)
                    tb2_output_file.close()
                Eopt_receptor=float(tb2out.split("\n")[-3].split()[3])
                if forced_output>0:
                    out_E.write(" "+str(Eopt_receptor))
                    pose_prot_1_opt=Pose()
                    pose_prot_1_opt.assign(load_assign(pose_prot_1,mut_folder+"/receptor.sol", INPUT_FOLDER+'receptor.resfile',scorefxn))
                    print("Receptor optimum Score:",scorefxn(pose_prot_1_opt),Eopt_receptor,'\n')
                    pose_prot_1_opt.dump_pdb(mut_folder+"/receptor.opt")
                    eshow(scorefxn,pose_prot_1_opt,mut_folder+'/receptor.show')
                    if temp == 0:
                        assert abs(scorefxn(pose_prot_1_opt)-Eopt_receptor)<= 0.01,"Score function scoring "+str(scorefxn(pose_prot_1_opt))+" and graph scoring "+str(Eopt_receptor)+" are different for receptor"
                progres += 1
                if verbosity ==0:
                    if Mut_on_lig == False:
                        printProgressBar(progres, l*2, prefix = 'Progress:', time=remaining_time, length = 50)
                    else:
                        printProgressBar(progres, l*3, prefix = 'Progress:', time=remaining_time,length = 50)
                end_mut_time=time.time()-start_mut_time
                outtime_file=open(outtime_name,"a")
                outtime_file.write(" "+str(end_mut_time))
                outtime_file.close()

            else:
                if forced_output>0:
                    out_E.write(" --- ")


            ##### FOR THE LIGAND
            if Mut_on_lig:
                start_mut_time=time.time()
                extract_sequence_file(pose_prot_2,mut_folder+"/ligand.seq",mut,nres_rec,nres_lig,nres_tot) # product ".seq" file for toulbar2 sequence mask        
                if verbosity > 0:
                    print("Launching toulbar2 on ligand.LG","with "+mut_folder+"/ligand.seq")
                                
                command=[toulbar2,INPUT_FOLDER+"ligand.LG",mut_folder+"/ligand.seq","-w="+mut_folder+"/ligand.sol"]
                if float(temp) >0:
                    command.append("-ztmp="+temp)
                if verbosity > 1:
                    print(command)
                tb2out=check_output(command,universal_newlines=True)
                Eopt_ligand=float(tb2out.split("\n")[-3].split()[3])
                if forced_output > 0:
                    out_E.write(" "+str(Eopt_ligand))
                    tb2_output_file=open(mut_folder+'/ligand.tb2','w')
                    tb2_output_file.write(tb2out)
                    tb2_output_file.close()
                    pose_prot_2_opt=Pose()
                    pose_prot_2_opt.assign(load_assign(pose_prot_2,mut_folder+"/ligand.sol", INPUT_FOLDER+'ligand.resfile',scorefxn))
                    if verbosity > 0:
                        print("Ligand optimum Score:",scorefxn(pose_prot_2_opt),Eopt_ligand,'\n')
                    pose_prot_2_opt.dump_pdb(mut_folder+"/ligand.opt")
                    eshow(scorefxn,pose_prot_2_opt,mut_folder+'/ligand.show')
                    if temp ==0 :
                        assert abs(scorefxn(pose_prot_2_opt)-Eopt_ligand)<= 0.01,"Score function scoring "+str(scorefxn(pose_prot_2_opt))+" and graph scoring "+str(Eopt_ligand)+" are different for ligand"

                progres += 1
                if verbosity ==0:
                    if Mut_on_rec==False:
                        printProgressBar(progres, l*2, prefix = 'Progress:', time=remaining_time, length = 50)
                    else:
                        printProgressBar(progres, l*3, prefix = 'Progress:', time=remaining_time, length = 50)
                end_mut_time=time.time()-start_mut_time
                outtime_file=open(outtime_name,"a")
                outtime_file.write(" "+str(end_mut_time))
                outtime_file.close()
            else:
                if forced_output > 0 :
                    out_E.write(" --- ")
                    
            #### FOR THE COMPLEX
            start_mut_time=time.time()
            extract_sequence_file(pose,mut_folder+"/"+mut+'.seq',mut,nres_rec,nres_lig,nres_tot) # product ".seq" file for toulbar2 sequence mask        
            if verbosity >0:
                print("Launching toulbar2 on "+mut+'.LG',"with "+mut_folder+"/"+mut+'.seq')
            command=[toulbar2,INPUT_FOLDER+"complex.LG",mut_folder+"/"+mut+'.seq',"-w="+mut_folder+"/"+mut+'.sol']
            if float(temp) >0:
                command.append("-ztmp="+temp)
            tb2out=check_output(command,universal_newlines=True)
            Eopt_complex=float(tb2out.split("\n")[-3].split()[3])
            if forced_output > 0 :
                out_E.write(" "+str(Eopt_complex)+"\n")
                tb2_output_file=open(mut_folder+'/'+mut+'.tb2','w')
                tb2_output_file.write(tb2out)
                tb2_output_file.close()              
                mut_pose=Pose()
                mut_pose.assign(load_assign(pose,mut_folder+"/"+mut+'.sol',INPUT_FOLDER+output_name+'.resfile',scorefxn))
                if verbosity > 0 :
                    print(mut,"optimum score:",scorefxn(mut_pose),Eopt_complex)
                mut_pose.dump_pdb(mut_folder+"/"+mut+".opt")
                eshow(scorefxn,mut_pose,mut_folder+'/'+mut+'.show')
                if temp == 0 :
                    assert abs(scorefxn(mut_pose)-Eopt_complex)<= 0.01,"Score function scoring "+str(scorefxn(mut_pose))+" and graph scoring "+str(Eopt_complex)+" are different for complex"
            if Mut_on_rec == False:
                delta = Eopt_complex-Eopt_ligand
            elif Mut_on_lig == False:
                delta = Eopt_complex-Eopt_receptor
            else:
                delta = Eopt_complex-Eopt_ligand-Eopt_receptor
            DeltaE.append([mut,delta])
            progres += 1
            if verbosity ==0:
                if Mut_on_lig == False or Mut_on_rec==False:
                    printProgressBar(progres, l*2, prefix = 'Progress:',  time=remaining_time, length = 50)
                else:
                    printProgressBar(progres, l*3, prefix = 'Progress:',  time=remaining_time, length = 50)

            end_mut_time=time.time()-start_mut_time
            if Mut_on_lig == True and Mut_on_rec == True:
                remaining_time=round(end_mut_time*(l-(progres/3)))
            else:
                remaining_time=round(end_mut_time*(l-(progres/2)))
            if verbosity > 0:
                print("Finished processing mutation:",mut,"DeltaE",delta,"in",hmsTime(round(end_mut_time)))
                print("Remaining time:",bcolors.BBLUE+hmsTime(remaining_time)+bcolors.ENDC,'\n')
            outtime_file=open(outtime_name,"a")
            outtime_file.write(" "+str(end_mut_time)+'\n')
            outtime_file.close()

    DeltaE_sort=sorted(DeltaE,key=itemgetter(1))
    for E_delta in DeltaE_sort:
        out_deltaE.write(E_delta[0]+' '+str(E_delta[1])+'\n')
    out_deltaE.close()
    if forced_output > 0 :
        out_E.close()
    
parser=optparse.OptionParser()
parser.add_option('--pdb', dest = 'pdb_file',
                  default = '',    
                  help = 'Protein complex structure in PDB format' )
parser.add_option('--seq', dest = 'seq_file',
                  default = '',    
                  help = 'Sequences to map' )
parser.add_option('--partner', dest = 'partner',
                  default = 'A_B',
                  help = 'partner chains\' name' )
parser.add_option('--score', dest='score' ,
                  default = "beta_nov16_soft",   
                  help = 'Scoring function: talaris2014, beta_nov15, beta_nov15_soft(default),etc...')  
parser.add_option('--temp', dest = 'temp',
                  default = 0,
                  help = 'Temperature' )    
parser.add_option('--min', dest='min_steps' ,
                  default = 0,
                  help = 'Number (default 0) of minimization (Quasi Newton) with tolerance 0.001')
parser.add_option('--cst', dest='cst' ,
                  default = 0.5,
                  help = 'Move backbone during minization under constraint with stdev of [--cst=float] (default 0.5)')
parser.add_option('--relax', dest = 'relax',
                  default = 0,
                  help = 'Number (default 0) of Fast Relax' )
parser.add_option('--v', dest = 'verbosity',
                  default = 0,
                  help = 'Set the verbosity level' )
parser.add_option('--ex1', dest = 'ex1',
                  default = 0,
                  help = 'Use the 1st extended version of Dunbrack library' )
parser.add_option('--ex2', dest = 'ex2',
                  default = 0,
                  help = 'Use the 2nd extended version of Dunbrack library' )
parser.add_option('--forced_out', dest = 'output_forced',
                  default = 0,
                  help = 'Options to get more output files' )
parser.add_option('--rec', dest = 'mutrec',
                  default = 0,
                  help = 'Options to force the computation of the matrix of the receptor' )
parser.add_option('--lig', dest = 'mutlig',
                  default = 0,
                  help = 'Options to force the computation of the matrix of the ligand' )
parser.add_option('--tb2_path', dest = 'toulbar2',
                  default = os.path.join(os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda:0))),"toulbar2"),
                  help = 'Options to set toulbar2 path' )
parser.add_option('--ignore_occupancy', dest = 'ignore_zero_occupancy',
                  default = 0,
                  help = 'Options to set toulbar2 path' )

(options,args) = parser.parse_args()

pdb_file=options.pdb_file
if  options.seq_file == '':
    seq_file = pdb_file.split(".pdb")[0]+".seq"
else:
    seq_file = options.seq_file
partner=options.partner
score=options.score
min_steps=int(options.min_steps)
bb_cst_sd=float(options.cst)
fast_relax=int(options.relax)
temp=options.temp
verbosity=int(options.verbosity)
ex1=int(options.ex1)
ex2=int(options.ex2)
forced_output=int(options.output_forced)
forced_mut_on_rec=int(options.mutrec)
forced_mut_on_lig=int(options.mutlig)
toulbar2=options.toulbar2

################# MUTATION, PDB and MATRIX PRODUCTION #############

if os.path.exists( os.getcwd() + '/' + pdb_file ) and pdb_file:
    start_time=time.time()
    print_options()
    output_name=pdb_file.split('.pdb')[0]
    ### Create output folder
    RESULT_FOLDER=os.getcwd()+"/"+output_name+"_"+score
    if min_steps > 0:
        RESULT_FOLDER= RESULT_FOLDER+"_min"
    if ex1==1:
        RESULT_FOLDER= RESULT_FOLDER + "_ex1"
    if ex2==1:
        RESULT_FOLDER = RESULT_FOLDER + "_ex2"
    RESULT_FOLDER += "/"                  
    if not os.path.exists(RESULT_FOLDER):
        os.mkdir(RESULT_FOLDER)
    outtime_name=RESULT_FOLDER+output_name+".time"
    if os.path.exists(outtime_name):
        os.remove(outtime_name)
    outtime_file=open(outtime_name,"a")
    rosetta_init()
    
    EasyE()

    end_time=round(time.time()-start_time)
    outtime_file=open(outtime_name,"a")
    outtime_file.write("Total "+str(end_time))
    outtime_file.close()
    print("\nTerminating:", hmsTime(end_time))
else:
    print(bcolors.BRED+"ERROR: PDB file not existing, exit..."+bcolors.ENDC)
