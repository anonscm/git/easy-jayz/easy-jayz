#!/usr/bin/env python3
import os
import time
import math
import os
import optparse
import re
import sys
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from Bio.PDB import *
from Bio.PDB import PDBParser
from Bio.PDB import PDBIO
import Bio.PDB
from optparse import OptionParser

#Parse the options
usage = "USAGE: python Interface-residue.py --pdb X.pdb --res residue_list [--c float] [--level string]\n"
parser = OptionParser(usage=usage)

#options
parser.add_option("--pdb",help="pdb", dest="pdb_file")
parser.add_option("--dist",help="Distance cutoff between contacting residues", dest="dist",default=3.0)
parser.add_option('--seq', dest = 'seq_file',default = '',    help = 'Sequences to map' )
parser.add_option("--level",help="set the level of the contact: all = All atom / CA = alpha carbon / noH = no hydrogen",dest="level",default="SC")
(options, args) = parser.parse_args()
pdb_file=options.pdb_file
seq_file = options.seq_file
dist=float(options.dist)
output_name=pdb_file.split('.pdb')[0]

pdb = PDBParser().get_structure(output_name,pdb_file)

sequences=[]
if os.path.exists( os.getcwd() + '/' + seq_file ) and seq_file:
  r = re.compile("([0-9]+)([a-zA-Z]+)")
  seqfile=open(seq_file,'r')
  for seq in seqfile.read().split('\n'):
    if len(seq) > 0 :
      name_pos=r.match(seq).groups()[0]
      sequences.append(int(name_pos))
else:
  print(bcolors.BRED+"ERROR: "+  os.getcwd() + '/' + seq_file+" file not existing, exit..."+bcolors.ENDC)
  exit(0)

io = PDBIO()
structure = PDBParser().get_structure('X', pdb_file) # load your molecule

#residue list
res_mut=sequences
res_mut_final=[]
res_protein_list = Selection.unfold_entities(structure, 'R')
for res in res_protein_list:
  for res_2 in res_mut:
    if res.get_id()[1] == int(res_2):
      res_mut_final.append(res)

chain_name=[]
chain_length=[]
for chain in structure.get_chains():
  chain_length.append(len(chain))
  chain_name.append(chain.get_id())

atom_list=[]
if options.level=="all":
  atom_list = Selection.unfold_entities(structure, 'A') # A for atom
elif options.level=="CA":
  res_list = Selection.unfold_entities(structure, 'R') # R for atom residue
  for res in res_list:
    atom_list.append(res["CA"])
elif options.level=="SC":
    atom_list = Selection.unfold_entities(structure, 'A') # A for atom
    noH_atom_list=[]
    for atom in atom_list:
        if not atom.element=='H' and not atom.get_name() == 'CA' and not atom.get_name() == 'C' and not atom.get_name() == 'O' and not atom.get_name() == 'N' :
            noH_atom_list.append(atom)
    atom_list=noH_atom_list
    
elif options.level=="noH":
  atom_list = Selection.unfold_entities(structure, 'A') # A for atom
  noH_atom_list=[]
  for atom in atom_list:
    if not atom.element=='H':
      noH_atom_list.append(atom)
  atom_list=noH_atom_list

ns = NeighborSearch(atom_list)
contact=[]

for residue_1 in res_mut_final:
  atom_list=[]
  for center in residue_1:
    center=center.get_coord()
    neighbors = ns.search(center, dist,level='R') # cutoff for distance in angstrom
    contact.append(neighbors)

contact_id=[]
for res_vect in contact:
  for res_full_id in res_vect:
    contact_id.append(res_full_id.get_id()[1])

flexibles=sorted(list(set(contact_id+res_mut)))
print("Flexible residues are:",flexibles)
