#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 01 16:20:00 2021
Last update Tue Nov 02 14:00:00 2021
@author: bouchiba

"""

from os.path import isfile
from pprint import pprint
import argparse

class Seq(object) :
    def __init__(self, fastafile = None):
        self.seqs = []
        if isfile(fastafile):
            self.load(fastafile)
        else:
            print('Error : You must provide a file')
            exit(1)
    def load(self, fastafile):
        with open( fastafile ) as f:
            content = f.read()
            lines = content.split('\n')
            for l in lines:
                if len(l)>0 and not l[0].startswith('>'):
                    self.seqs.append(l.strip())
            f.close()

def substitutionFinder(ref, mut):
    for i,muts in enumerate(mut.seqs):
        substName=[]
        if (len(ref.seqs[0]) == len(muts)):
            for j,c in enumerate(muts):
                if (ref.seqs[0][j] != c):
                    substName.append(str(j+1)+c)
            print("_".join(substName))
        else:
            print("Different sequence lengths.")
            exit(1)                

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='writes the substitution names in the format [Native res]{number}[Mutant res], it will only work is the two sequences have the same length.')
    parser.add_argument('--ref', required=True, help='native sequence \n use one line fasta format.')
    parser.add_argument('--mut',required=True,help='mutant sequence \n use one line fasta format (one line per sequence)')
    param = parser.parse_args()
    referenceSeq=Seq(param.ref)
    mutantSeq=Seq(param.mut)
    if isfile(param.ref) and isfile(param.mut):
        substitutionFinder(referenceSeq,mutantSeq)
