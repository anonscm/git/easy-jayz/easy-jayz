#!/bin/bash

git clone https://github.com/toulbar2/toulbar2.git tb2
cd tb2
git checkout cpd
mkdir build
cd build
cmake ..
make -j$(nproc --all)
cd ../../
rm -f toulbar2
ln -s tb2/build/bin/Linux/toulbar2

